## Buildroot setup for BeagleBone AI-64 board

### Table of Contents

- [Getting started](#getting-started)
- [System configuration](#system-configuration)
- [TI Processor SDK](#ti-processor-sdk)
- [TDA4VM hardware accelerators](#tda4vm-hardware-accelerators)
- [TI EdgeAI](#ti-edgeai)

### Getting started

1. Clone repository  
    ```
    git clone --recurse-submodules https://gitlab.com/b_bloodhound_h/bloodhound.git
    ```

2. Create build directory and initialize buildroot using external configuration  
    ```
    cd bloodhound
    mkdir build && cd buildroot
    make BR2_EXTERNAL=../bloodhound O=../build bb-ai64_defconfig
    ```
    Now the directory tree looks like this:  
    ```
    ├── bloodhound
    │   ├── ...
    ├── build
    │   ├── ...
    └── buildroot <-- you are here
        └── ...
    ```

3. Build SD card image  
    ```
    cd ../build && make all
    ```

4. Flash sdcard.img to SD card  
    Flash sdcard.img (```build/images/sdcard.img```) to SD card using any convenient utility.

5. Connect to BB AI-64  
    Read [Quick Start Guide](https://www.genome.gov/) for BeagleBone AI-64.  
    For a system built earlier, you need:  
      * 5V @ 3A barrel jack power adapter
      * USB-C cable  

    Connect the USB-C cable as described [here](https://docs.beagleboard.org/latest/boards/beaglebone/ai-64/02-quick-start.html#connect-the-cable-to-the-board).  
    By default, the USB-C port is configured in USB CDC-ECM device mode, after connecting you  
    should see a **new network interface**.  
    You **must** configure the IP for this interface in the 192.168.14.* subnet manually. Like this:  
    ```
    sudo ifconfig <if> 192.168.14.100 netmask 255.255.255.0
    ```
    Board IP is **192.168.14.88**. You can use ssh to connect to the board:
    ```
    ssh user@192.168.14.88
    ```
    Default user: ```user```  
    Default password: ```user```


### System configuration

- Linux Kernel  
    Linux Kernel version is **6.1.69-ti-arm64-r30** from beaglebone [repo](https://openbeagle.org/beagleboard/linux).  
    Additional patches are applied (see ```bloodhound/board/beaglebone-ai64/linux-patches```)  
    to enable USB-C, support a wide range of SD cards and apply Vision Apps memory mapping.  
    Also check ```bloodhound/board/beaglebone-ai64/extlinux.conf``` file, edit this  
    configuration if necessary (add cmdline args, dts overlays, etc).

- USB-C device mode  
    To configure USB-C device mode ```bloodhound/package/usb-gadget``` package is used.

- Storage  
    On first startup, the ```resizefs.sh``` script is run to increase the rootfs size to all available space on  
    the SD card. See package ```bloodhound/package/resize-sdcard```.

### TI Processor SDK

The ```bloodhound/package/ti-processor-sdk``` package contains precompiled binaries and headers  
from ti-processor-sdk-rtos-j721e-evm version **09.01.00.06**.  
The package is enabled by default and is **required** for all EdgeAI packages (see below) and hardware  
accelerators.

### TDA4VM hardware accelerators

The ```bloodhound/package/ti-img-encode-decode``` package contains firmware for TI video  
encode/decode v4l2 drivers (enabled in kernel config).  
The ```bloodhound/package/ti-processor-sdk``` package contains Vision Apps firmware files for  
R5 cores, C6x and C7x DSP co-processors (builded **without** peripheral access).

### TI EdgeAI

Available plugins and apps:  
- ti-edgeai-apps-utils
- ti-edgeai-tiovx-kernels
- ti-edgeai-tiovx-modules
- ti-edgeai-gst-plugins  

EdgeAI requirements:
- Vision Apps memory map (see ```bloodhound/board/beaglebone-ai64/linux-patches/0003-add-vision-apps-memory-mapping.patch```)
- Vision Apps firmware (see ```bloodhound/package/ti-processor-sdk/source/vision_apps/firmware```)

Test gstreamer plugins:
```
gst-inspect-1.0 ti
gst-inspect-1.0 tiovx
```
**Note**: you must run gstreamer pipelines with tiovx plugins as **root**.
