################################################################################
#
# gadget-tool
#
################################################################################

GADGET_TOOL_VERSION = 1.0
GADGET_TOOL_SITE = $(call github,linux-usb-gadgets,gt,7f9c45d)
GADGET_TOOL_LICENSE = Apache License 2.0
GADGET_TOOL_LICENSE_FILES = LICENSE
GADGET_TOOL_SUBDIR = source
GADGET_TOOL_DEPENDENCIES = libusbgx libconfig libglib2

$(eval $(cmake-package))
