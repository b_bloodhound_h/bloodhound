################################################################################
#
# graphviz
#
################################################################################

GRAPHVIZ_VERSION = 10.0.1
GRAPHVIZ_SITE = https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/$(GRAPHVIZ_VERSION)
GRAPHVIZ_SOURCE = graphviz-$(GRAPHVIZ_VERSION).tar.gz
GRAPHVIZ_LICENSE = CPL
GRAPHVIZ_LICENSE_FILES = COPYING
HOST_GRAPHVIZ_DEPENDENCIES = host-libzlib host-gd host-swig
HOST_GRAPHVIZ_CONF_OPTS = --disable-perl

$(eval $(host-autotools-package))
