#!/bin/bash

set -e

# get root partition from kernel cmdline
root_dev="$(/usr/bin/sed -ne 's/.*\broot=\([^ ]*\)\b.*/\1/p' < /proc/cmdline)"
block_dev="$(/usr/bin/lsblk -no pkname ${root_dev})"
root_dev="$(/usr/bin/echo $root_dev | /usr/bin/sed 's/^.....//g')"
partition_num="$(/usr/bin/cat /sys/block/${block_dev}/${root_dev}/partition)"

# resize root partition (in table)
/usr/sbin/parted /dev/$block_dev resizepart $partition_num 100%
# inform kernel about changes
/usr/sbin/partprobe /dev/$block_dev
# actual resizing of root partition
/usr/sbin/resize2fs /dev/$root_dev

echo "Resized successfully!"
exit 0
