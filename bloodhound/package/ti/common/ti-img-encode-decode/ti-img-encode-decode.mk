################################################################################
#
# ti-img-encode-decode
#
################################################################################

TI_IMG_ENCODE_DECODE_VERSION = origin/master
TI_IMG_ENCODE_DECODE_SITE = git://git.ti.com/jacinto7_multimedia/ti-img-encode-decode.git
TI_IMG_ENCODE_DECODE_SITE_METHOD = git

define TI_IMG_ENCODE_DECODE_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/decoder/firmware/pvdec_full_bin.fw $(TARGET_DIR)/lib/firmware/pvdec_full_bin.fw
endef

$(eval $(generic-package))
