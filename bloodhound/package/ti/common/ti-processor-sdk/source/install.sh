#!/bin/bash

mkdir -p $TARGET_DIR/usr/include/
mkdir -p $TARGET_DIR/usr/lib/pkgconfig/
mkdir -p $TARGET_DIR/usr/lib/firmware/

for package in $(ls -d $CURRENT_DIR/*/)
do
    echo "Install $package"
    if [ -d $package/include ]; then
        rsync -R -a --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r \
              $package/include/./ $TARGET_DIR/usr/include/
    fi
    if [ -d $package/lib ]; then
        rsync -R -a --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rwx,Fg=rx,Fo=rx \
              $package/lib/./ $TARGET_DIR/usr/lib/
    fi
    if [ -d $package/pkgconfig ]; then
        rsync -R -a --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r \
              $package/pkgconfig/./ $TARGET_DIR/usr/lib/pkgconfig/
    fi

    if [ -d $package/firmware ]; then
        rsync -R -a --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r \
              $package/firmware/./ $TARGET_DIR/usr/lib/firmware/
    fi
done
