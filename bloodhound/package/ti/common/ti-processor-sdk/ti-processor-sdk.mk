################################################################################
#
# ti-processor-sdk
#
################################################################################

TI_PROCESSOR_SDK_VERSION = 09.01.00.06
TI_PROCESSOR_SDK_SITE = $(BR2_EXTERNAL_bloodhound_PATH)/package/ti/common/ti-processor-sdk/source
TI_PROCESSOR_SDK_SITE_METHOD = local
TI_PROCESSOR_SDK_INSTALL_STAGING = YES

define TI_PROCESSOR_SDK_INSTALL_TARGET_CMDS
    CURRENT_DIR=$(@D) TARGET_DIR=$(TARGET_DIR) $(@D)/install.sh
endef

define TI_PROCESSOR_SDK_INSTALL_STAGING_CMDS
    CURRENT_DIR=$(@D) TARGET_DIR=$(STAGING_DIR) $(@D)/install.sh
endef

$(eval $(generic-package))
