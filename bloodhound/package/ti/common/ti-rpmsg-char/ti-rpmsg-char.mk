################################################################################
#
# ti-rpmsg-char
#
################################################################################

TI_RPMSG_CHAR_VERSION = origin/master
TI_RPMSG_CHAR_SITE = git://git.ti.com/rpmsg/ti-rpmsg-char.git
TI_RPMSG_CHAR_SITE_METHOD = git
TI_RPMSG_CHAR_INSTALL_STAGING = YES
TI_RPMSG_CHAR_AUTORECONF = YES

$(eval $(autotools-package))
