################################################################################
#
# ti-edgeai-apps-utils
#
################################################################################

TI_EDGEAI_APPS_UTILS_VERSION = 09.01.00.06
TI_EDGEAI_APPS_UTILS_SITE = git://git.ti.com/edgeai/edgeai-apps-utils.git
TI_EDGEAI_APPS_UTILS_SITE_METHOD = git
TI_EDGEAI_APPS_UTILS_INSTALL_STAGING = YES

TI_EDGEAI_APPS_UTILS_CONF_ENV += SOC=j721e
TI_EDGEAI_APPS_UTILS_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release

$(eval $(cmake-package))
