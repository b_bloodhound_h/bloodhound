################################################################################
#
# ti-edgeai-gst-plugins
#
################################################################################

TI_EDGEAI_GST_PLUGINS_VERSION = 09.01.00.06
TI_EDGEAI_GST_PLUGINS_SITE = $(call github,TexasInstruments,edgeai-gst-plugins,09.01.00.06)
TI_EDGEAI_GST_PLUGINS_LICENSE = Limited License
TI_EDGEAI_GST_PLUGINS_LICENSE_FILES = LICENSE
TI_EDGEAI_GST_PLUGINS_DEPENDENCIES = ti-edgeai-tiovx-modules host-pkgconf gstreamer1 libxcb xlib_libxshmfence wayland

TI_EDGEAI_GST_PLUGINS_CONF_ENV += SOC=j721e
TI_EDGEAI_GST_PLUGINS_CONF_OPTS += -Dtests="disabled" -Dexamples="disabled" -Ddoc="disabled" -Dprofiling="disabled" -Ddl-plugins="disabled" -Denable-tidl="disabled"

$(eval $(meson-package))
