################################################################################
#
# ti-edgeai-tiovx-kernels
#
################################################################################

TI_EDGEAI_TIOVX_KERNELS_VERSION = 09.01.00.06
TI_EDGEAI_TIOVX_KERNELS_SITE = git://git.ti.com/edgeai/edgeai-tiovx-kernels.git
TI_EDGEAI_TIOVX_KERNELS_SITE_METHOD = git
TI_EDGEAI_TIOVX_KERNELS_INSTALL_STAGING = YES
TI_EDGEAI_TIOVX_KERNELS_DEPENDENCIES = ti-edgeai-apps-utils

TI_EDGEAI_TIOVX_KERNELS_CONF_ENV += SOC=j721e
TI_EDGEAI_TIOVX_KERNELS_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release

$(eval $(cmake-package))
