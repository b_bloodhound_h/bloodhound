################################################################################
#
# ti-edgeai-tiovx-modules
#
################################################################################

TI_EDGEAI_TIOVX_MODULES_VERSION = 09.01.00.06
TI_EDGEAI_TIOVX_MODULES_SITE = git://git.ti.com/edgeai/edgeai-tiovx-modules.git
TI_EDGEAI_TIOVX_MODULES_SITE_METHOD = git
TI_EDGEAI_TIOVX_MODULES_INSTALL_STAGING = YES
TI_EDGEAI_TIOVX_MODULES_DEPENDENCIES = ti-edgeai-apps-utils ti-edgeai-tiovx-kernels

TI_EDGEAI_TIOVX_MODULES_CONF_ENV += SOC=j721e
TI_EDGEAI_TIOVX_MODULES_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release

$(eval $(cmake-package))
