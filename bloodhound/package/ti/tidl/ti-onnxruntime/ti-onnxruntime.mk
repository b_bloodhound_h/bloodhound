################################################################################
#
# ti-onnxruntime
#
################################################################################

TI_ONNXRUNTIME_VERSION = 1.14.0
TI_TIDL_TOOLS_VERSION = 09_01_06_00
TI_ONNXRUNTIME_SITE = https://software-dl.ti.com/jacinto7/esd/tidl-tools/$(TI_TIDL_TOOLS_VERSION)/OSRT_TOOLS/ARM_LINUX/ARAGO
TI_ONNXRUNTIME_SOURCE = onnx_$(TI_ONNXRUNTIME_VERSION)_aragoj7.tar.gz

define TI_ONNXRUNTIME_INSTALL_TARGET_CMDS
    cp $(@D)/libonnxruntime.so.1.14.0  ${TARGET_DIR}/usr/lib/libonnxruntime.so.1.14.0
    ln -s libonnxruntime.so.1.14.0 ${TARGET_DIR}/usr/lib/libonnxruntime.so
endef

$(eval $(generic-package))
