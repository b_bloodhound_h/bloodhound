################################################################################
#
# ti-python-onnxruntime
#
################################################################################

TI_PYTHON_ONNXRUNTIME_VERSION = 1.14.0
TI_TIDL_TOOLS_VERSION = 09_01_06_00
PYTHON_VERSION_MAJOR = 3.10
TI_PYTHON_ONNXRUNTIME_SITE = https://software-dl.ti.com/jacinto7/esd/tidl-tools/$(TI_TIDL_TOOLS_VERSION)/OSRT_TOOLS/ARM_LINUX/ARAGO
TI_PYTHON_ONNXRUNTIME_SOURCE = onnxruntime_tidl-$(TI_PYTHON_ONNXRUNTIME_VERSION)-cp310-cp310-linux_aarch64.whl
TI_PYTHON_ONNXRUNTIME_DEPENDENCIES = host-python-pip

# Copy .whl file to build dir
define TI_PYTHON_ONNXRUNTIME_EXTRACT_CMDS
    cp $(TI_PYTHON_ONNXRUNTIME_DL_DIR)/$(TI_PYTHON_ONNXRUNTIME_SOURCE) $(@D)
endef

define TI_PYTHON_ONNXRUNTIME_INSTALL_TARGET_CMDS
    ($(TARGET_CONFIGURE_OPTS) \
    $(HOST_DIR)/usr/bin/pip install -v --no-deps --platform linux_aarch64 $(@D)/$(TI_PYTHON_ONNXRUNTIME_SOURCE) \
    --target $(TARGET_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages --disable-pip-version-check)
endef

$(eval $(generic-package))
