import onnx
import onnxruntime as rt
import time
import os
import numpy as np
import cv2
import argparse
import json

tidl_tools_path = os.environ["TIDL_TOOLS_PATH"]
model_pkg_path = os.environ["MODEL_PKG_PATH"]
artifacts_folder = os.path.join(model_pkg_path, "artifacts")
SOC = os.environ["SOC"]

#### DEFAULT COMPILATION PARAMETERS ####

tensor_bits = 8
debug_level = 6
max_num_subgraphs = 16
accuracy_level = 1
calibration_frames = 2
calibration_iterations = 5
output_feature_16bit_names_list = ""
params_16bit_names_list = ""
mixed_precision_factor = -1
quantization_scale_type = 0
high_resolution_optimization = 0
pre_batchnorm_fold = 1
inference_mode = 0
num_cores = 1
ti_internal_nc_flag = 1601

data_convert = 3
if (quantization_scale_type == 3):
    data_convert = 0

# set to default accuracy_level 1
activation_clipping = 1
weight_clipping = 1
bias_calibration = 1
channel_wise_quantization = 0

optional_options = {
    # "priority":0,
    #delay in ms
    # "max_pre_empt_delay":10
    "platform": "J7",
    "version": "7.2",
    "tensor_bits": tensor_bits,
    "debug_level": debug_level,
    "max_num_subgraphs": max_num_subgraphs,
    "deny_list": "",
    "deny_list:layer_type": "",
    "deny_list:layer_name": "",
    "model_type": "",
    "accuracy_level": accuracy_level,
    "advanced_options:calibration_frames": calibration_frames,
    "advanced_options:calibration_iterations": calibration_iterations,
    "advanced_options:output_feature_16bit_names_list": output_feature_16bit_names_list,
    "advanced_options:params_16bit_names_list": params_16bit_names_list,
    "advanced_options:mixed_precision_factor":  mixed_precision_factor,
    "advanced_options:quantization_scale_type": quantization_scale_type,
    # "object_detection:meta_layers_names_list" : meta_layers_names_list,  -- read from models_configs dictionary below
    # "object_detection:meta_arch_type" : meta_arch_type,                  -- read from models_configs dictionary below
    "advanced_options:high_resolution_optimization": high_resolution_optimization,
    "advanced_options:pre_batchnorm_fold": pre_batchnorm_fold,
    "ti_internal_nc_flag": ti_internal_nc_flag,
    # below options will be read only if accuracy_level = 9, else will be discarded.... for accuracy_level = 0/1, these are preset internally
    "advanced_options:activation_clipping": activation_clipping,
    "advanced_options:weight_clipping": weight_clipping,
    "advanced_options:bias_calibration": bias_calibration,
    "advanced_options:add_data_convert_ops": data_convert,
    "advanced_options:channel_wise_quantization": channel_wise_quantization,
    # Advanced options for SOC 'am69a'
    "advanced_options:inference_mode": inference_mode,
    "advanced_options:num_cores": num_cores
}

required_options = {
    "tidl_tools_path": tidl_tools_path,
    "artifacts_folder": artifacts_folder
}

#########################################

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', type=str, help='Model config file')
args = parser.parse_args()
os.environ["TIDL_RT_PERFSTATS"] = "1"

so = rt.SessionOptions()
so.log_severity_level = 3

if(SOC == 'am62'):
    print('ERROR: Unsupported SOC')
    exit(-1)


def infer_image(sess, image_files, config):
    input_details = sess.get_inputs()
    input_name = input_details[0].name
    floating_model = (input_details[0].type == 'tensor(float)')
    height = input_details[0].shape[2]
    width = input_details[0].shape[3]
    channel = input_details[0].shape[1]
    batch = input_details[0].shape[0]
    imgs = []
    shape = [batch, channel, height, width]
    input_data = np.zeros(shape)

    for i in range(batch):
        img = cv2.imread(image_files[i], cv2.IMREAD_COLOR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (height, width), interpolation=cv2.INTER_AREA)
        imgs.append(img)
        temp_input_data = np.expand_dims(img, axis=0)
        temp_input_data = np.transpose(temp_input_data, (0, 3, 1, 2))
        input_data[i] = temp_input_data[0]
    if floating_model:
        input_data = np.float32(input_data)
        for mean, scale, ch in zip(config['mean'], config['scale'], range(input_data.shape[1])):
            input_data[:, ch, :, :] = (
                (input_data[:, ch, :, :] - mean) * scale)

    print(
        f'Float: {floating_model}, size: {width}x{height}, channel: {channel}, batch: {batch}\n')
    start_time = time.time()
    output = list(sess.run(None, {input_name: input_data}))
    stop_time = time.time()
    infer_time = stop_time - start_time
    print(f'Infer time: {infer_time}')

    return imgs, output, height, width


def get_onnx_opset_version(model_path):
    model = onnx.load(model_path)
    return model.opset_import[0].version if len(model.opset_import) > 0 else None


def run_model(config):
    print('Available execution providers : ', rt.get_available_providers())
    print('Running_Model : ', config['model_path'])

    config['model_path'] = os.path.join(model_pkg_path, config['model_path'])

    opset_version = get_onnx_opset_version(config['model_path'])
    if opset_version is None:
        print('WARNING: Cannot identify OpSet version')
    elif opset_version > 9:
        print(
            f'ERROR: Unsupported OpSet version, supported <=9, actual {opset_version}')

    print(f'ONNX OpSet version {opset_version}')

    onnx.checker.check_model(config['model_path'], full_check=True)
    onnx.shape_inference.infer_shapes_path(
        config['model_path'], config['model_path'])

    delegate_options = {}
    delegate_options.update(required_options)
    delegate_options.update(optional_options)
    if 'optional_options' in config:
        delegate_options.update(config['optional_options'])

    # delete the contents of artifacts folder
    os.makedirs(delegate_options['artifacts_folder'], exist_ok=True)
    for root, dirs, files in os.walk(delegate_options['artifacts_folder'], topdown=False):
        [os.remove(os.path.join(root, f)) for f in files]
        [os.rmdir(os.path.join(root, d)) for d in dirs]

    calib_path = os.path.join(model_pkg_path, 'calib_data')
    if not os.path.exists(calib_path):
        print(
            f'ERROR: Cannot find calibration data, {calib_path} does not exist')
        exit(-1)
    calib_data = [os.path.join(calib_path, f)
                  for f in sorted(os.listdir(calib_path))]
    if not calib_data:
        print(f'ERROR: Calibration data is empty')
        exit(-1)

    input_data = calib_data
    print(f'Input data: {input_data}\n')

    numFrames = config['num_images']
    if numFrames > delegate_options['advanced_options:calibration_frames']:
        numFrames = delegate_options['advanced_options:calibration_frames']

    EP_list = ['TIDLCompilationProvider', 'CPUExecutionProvider']
    sess = rt.InferenceSession(config['model_path'], providers=EP_list,
                               provider_options=[delegate_options, {}], sess_options=so)

    # run session
    for i in range(numFrames):
        start_index = i % len(input_data)
        input_details = sess.get_inputs()
        batch = input_details[0].shape[0]
        input_images = []
        # for batch processing diff image needed for a single  input
        for j in range(batch):
            input_images.append(input_data[(start_index+j) % len(input_data)])
        print(f'Run {i+1}, images: {input_images}\n')
        imgs, output, height, width = infer_image(sess, input_images, config)

    del sess


def get_config(config_path):
    config = {}
    with open(config_path, 'r') as config_file:
        config = json.load(config_file)
    return config


config_path = os.path.join(model_pkg_path, args.config)
config = get_config(config_path)
if not config:
    print(f'ERROR: Config ({config_path}) is empty')
    exit(-1)
run_model(config)
