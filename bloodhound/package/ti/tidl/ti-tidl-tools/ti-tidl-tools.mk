################################################################################
#
# ti-tidl-tools
#
################################################################################

TI_TIDL_TOOLS_VERSION = 09_01_06_00
TI_TIDL_TOOLS_SOC = AM68PA
PYTHON_VERSION_MAJOR = 3.10
TI_TIDL_TOOLS_SITE = $(BR2_EXTERNAL_bloodhound_PATH)/package/ti/tidl/ti-tidl-tools/source
TI_TIDL_TOOLS_SITE_METHOD = local
TI_TIDL_TOOLS_DEPS_SITE = https://software-dl.ti.com/jacinto7/esd/tidl-tools/$(TI_TIDL_TOOLS_VERSION)
HOST_TI_TIDL_TOOLS_DEPENDENCIES = host-python-pip host-python3

define TI_TIDL_TOOLS_DOWNLOAD
    $(WGET) -np -nH -nc -P $($(PKG)_DL_DIR) \
        $(TI_TIDL_TOOLS_DEPS_SITE)/TIDL_TOOLS/$(TI_TIDL_TOOLS_SOC)/tidl_tools.tar.gz
    $(WGET) -np -nH -nc -P $($(PKG)_DL_DIR) \
        $(TI_TIDL_TOOLS_DEPS_SITE)/OSRT_TOOLS/X86_64_LINUX/UBUNTU_22_04/onnx_1.14.0_x86_u22.tar.gz
    $(WGET) -np -nH -nc -P $($(PKG)_DL_DIR) \
        $(TI_TIDL_TOOLS_DEPS_SITE)/OSRT_TOOLS/X86_64_LINUX/UBUNTU_22_04/opencv_4.2.0_x86_u22.tar.gz
    $(WGET) -np -nH -nc -P $($(PKG)_DL_DIR) \
        $(TI_TIDL_TOOLS_DEPS_SITE)/OSRT_TOOLS/X86_64_LINUX/UBUNTU_22_04/onnxruntime_tidl-1.14.0-cp310-cp310-linux_x86_64.whl
    rm -rf $(@D)/tidl_tools
    tar -xzf $($(PKG)_DL_DIR)/tidl_tools.tar.gz -C $(@D)/
    mkdir -p $(@D)/tidl_tools/osrt_deps
    tar -xzf $($(PKG)_DL_DIR)/onnx_1.14.0_x86_u22.tar.gz -C $(@D)/tidl_tools/osrt_deps/
    ln -s libonnxruntime.so.1.14.0 $(@D)/tidl_tools/osrt_deps/onnx_1.14.0_x86_u22/libonnxruntime.so
    tar -xzf $($(PKG)_DL_DIR)/opencv_4.2.0_x86_u22.tar.gz -C $(@D)/tidl_tools/osrt_deps/
    cp $($(PKG)_DL_DIR)/onnxruntime_tidl-1.14.0-cp310-cp310-linux_x86_64.whl $(@D)/
endef
HOST_TI_TIDL_TOOLS_POST_RSYNC_HOOKS += TI_TIDL_TOOLS_DOWNLOAD

define HOST_TI_TIDL_TOOLS_INSTALL_CMDS
    sed -i "s,RP_PYTHON_INTERPRETER_PATH,PYTHON_INTERPRETER_PATH=$(HOST_DIR)/bin," $(@D)/tidl_compile_onnx.sh
    sed -i "s,RP_COMPILE_PYSCRIPT_PATH,COMPILE_PYSCRIPT_PATH=$(HOST_DIR)/usr/share/tidl_tools," $(@D)/tidl_compile_onnx.sh
    sed -i "s,EXPORT_SOC,export SOC=$(call LOWERCASE,$(TI_TIDL_TOOLS_SOC))," $(@D)/tidl_compile_onnx.sh
    sed -i "s,EXPORT_TIDL_TOOLS_PATH,export TIDL_TOOLS_PATH=$(HOST_DIR)/usr/share/tidl_tools," $(@D)/tidl_compile_onnx.sh

    ($(HOST_CONFIGURE_OPTS) \
     $(HOST_DIR)/usr/bin/pip install -v --no-deps --platform linux_x86_64 $(@D)/onnxruntime_tidl-1.14.0-cp310-cp310-linux_x86_64.whl \
     --target $(HOST_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages --disable-pip-version-check --no-cache-dir)
    ($(HOST_CONFIGURE_OPTS) \
     $(HOST_DIR)/usr/bin/pip install -v -r $(@D)/requirements_pc.txt \
     --target $(HOST_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages --disable-pip-version-check --no-cache-dir)

    rm -rf $(HOST_DIR)/usr/share/tidl_tools
    cp -r $(@D)/tidl_tools $(HOST_DIR)/usr/share/
    cp $(@D)/tidl_compile_onnx.py $(HOST_DIR)/usr/share/tidl_tools/
    install -m 755 $(@D)/tidl_compile_onnx.sh $(HOST_DIR)/bin
endef

$(eval $(host-generic-package))
