#!/bin/bash

set -e

GADGET_NAME=ug1
GADGET_SCHEME=usb-gadget.scheme
GADGET_CONFIG=/sys/kernel/config/usb_gadget/${GADGET_NAME}
NET_IPv4=192.168.14.88
GT_APP=/usr/bin/gt
IFCONFIG_APP=/usr/sbin/ifconfig

if [ ${1} = "start" ]; then
    ${GT_APP} load ${GADGET_SCHEME} ${GADGET_NAME}
    ifname=$(cat ${GADGET_CONFIG}/functions/ecm.usb0/ifname)
    ${IFCONFIG_APP} ${ifname} ${NET_IPv4} netmask 255.255.255.0 up
elif [ ${1} = "stop" ]; then
    ifname=$(cat ${GADGET_CONFIG}/functions/ecm.usb0/ifname)
    ${IFCONFIG_APP} ${ifname} down
    ${GT_APP} rm -rf ${GADGET_NAME}
fi

exit 0
