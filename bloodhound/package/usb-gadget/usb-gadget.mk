################################################################################
#
# usb-gadget
#
################################################################################

USB_GADGET_VERSION = 1.0
USB_GADGET_SITE = $(BR2_EXTERNAL_bloodhound_PATH)/package/usb-gadget/source
USB_GADGET_SITE_METHOD = local

define USB_GADGET_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0644 $(@D)/99-usb-gadget.rules $(TARGET_DIR)/etc/udev/rules.d/99-usb-gadget.rules
    $(INSTALL) -D -m 0644 $(@D)/usb-gadget.service $(TARGET_DIR)/etc/systemd/system/usb-gadget.service
    $(INSTALL) -D -m 0644 $(@D)/usb-gadget.target $(TARGET_DIR)/etc/systemd/system/usb-gadget.target
    $(INSTALL) -D -m 0644 $(@D)/usb-gadget.scheme $(TARGET_DIR)/etc/gt/templates/usb-gadget.scheme
    $(INSTALL) -D -m 0755 $(@D)/usb-gadget.sh $(TARGET_DIR)/usr/bin/usb-gadget.sh
endef

$(eval $(generic-package))
